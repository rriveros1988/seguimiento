<?php
    header("Content-Type: text/html;charset=utf-8");
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="sintomasPersonal.xlsx"');
    require("PHPExcel.php");
    require('../model/consultas.php');
    date_default_timezone_set('America/Santiago');
    session_start();
    setcookie("tk_w_o",$_COOKIE["tk_w_o"],time()+300);
    actualizaTokenLogin($_SESSION['rutUser'], $_COOKIE["tk_w_o"]);

    $rutUser = $_SESSION['rutUser'];
    $fecha = $_GET['fecha'];
    $permisos = consultaAreasComunes($rutUser);

    $permisoPersonal = 0;
    for($i = 0; $i < count($permisos); $i++){
      if($permisos[$i]['NOMBRE'] == 'buttonMiPersonal'){
        $permisoPersonal = $permisos[$i]['TODOS'];
      }
    }

    if($permisoPersonal == 1){
        $row = consultaPersonalSintomasTodos($fecha);
    }
    else{
        $row = consultaPersonalSintomas($fecha,$rutUser);
    }

    $detalle = array();
    $detalle[] = array('DNI','NOMBRE','TEMPERATURA AM','TOS AM','DOLOR DE GARGANTA AM','DOLOR MUSCULAR AM','DIFICULTAD PARA RESPIRAR AM', 'SECRECION NASAL AM', 'MANCHAS PIEL AM','DOLOR CABEZA AM','NAUSEA VOMITO AM','DOLOR ARTICULAR AM','TEMPERATURA PM','TOS PM','DOLOR DE GARGANTA PM','DOLOR MUSCULAR PM','DIFICULTAD PARA RESPIRAR PM', 'SECRECION NASAL PM', 'MANCHAS PIEL PM','DOLOR CABEZA PM','NAUSEA VOMITO PM','DOLOR ARTICULAR PM');

    for($i = 0; $i < count($row); $i++){
        $detalle[] = array($row[$i]['RUT'],$row[$i]['NOMBRE'],$row[$i]['TEMP_AM'],$row[$i]['TOS_AM'],$row[$i]['DOLOR_GARGANTA_AM'],$row[$i]['DOLOR_MUS_AM'],$row[$i]['DIF_RESPIRAR_AM'],$row[$i]['SEC_NASAL_AM'],$row[$i]['MAN_PIEL_AM'],$row[$i]['DOLOR_CAB_AM'],$row[$i]['NAU_VOM_AM'],$row[$i]['DOLOR_ART_AM'],$row[$i]['TEMP_PM'],$row[$i]['TOS_PM'],$row[$i]['DOLOR_GARGANTA_PM'],$row[$i]['DOLOR_MUS_PM'],$row[$i]['DIF_RESPIRAR_PM'],$row[$i]['SEC_NASAL_PM'],$row[$i]['MAN_PIEL_PM'],$row[$i]['DOLOR_CAB_PM'],$row[$i]['NAU_VOM_PM'],$row[$i]['DOLOR_ART_PM']);
    }

    //Generamos archivo excel
    $excel = new PHPExcel();

    // Fill worksheet from values in array
    $excel->getActiveSheet()->fromArray($detalle, null, 'A1');

    // Rename worksheet
    $excel->getActiveSheet()->setTitle("Sintomas");

    // Set AutoSize for name and email fields
    $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);

    $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $writer->save('php://output');
?>
