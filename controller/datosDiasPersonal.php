<?php
  require('../model/consultas.php');
  session_start();
  setcookie("tk_w_o",$_COOKIE["tk_w_o"],time()+300);
  actualizaTokenLogin($_SESSION['rutUser'], $_COOKIE["tk_w_o"]);

	if(count($_POST) >= 0){
        $rutUser = $_SESSION['rutUser'];

        $row = consultaDiasDatosPersonal();

        if(is_array($row))
        {
            $results = array(
                "sEcho" => 1,
                "iTotalRecords" => count($row),
                "iTotalDisplayRecords" => count($row),
                "aaData"=>$row
            );
            echo json_encode($results);
        }
        else{
            $results = array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData"=>[]
            );
            echo json_encode($results);
        }
	}
	else{
		echo "Sin datos";
	}
?>
