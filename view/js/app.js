var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/personal", {
        controller: "personalController",
        controllerAs: "vm",
        templateUrl : "view/personal/personal.html"
    })
    .when("/seguimiento", {
        controller: "seguimientoController",
        controllerAs: "vm",
        templateUrl : "view/personal/seguimiento.html"
    })
    .otherwise({redirectTo: '/personal'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
        $('#contenido').fadeIn();
        $('#footer').fadeIn();
        $('#menu-lateral').fadeIn();
        $("#menu-lateral").css("width","45px");
        // $('#menu-lateral').hover(function(){
        //     $("#menu-lateral").css("width","200px");
        // },
        // function() {
        //     $("#menu-lateral").css("width","45px");
        // });
        setTimeout(function(){
          $('#modalAlertasSplash').modal('hide');
        },500);
        $("#loginSystem").show("slide", {direction: "up"}, 800);
    },1500);
});

app.controller("personalController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $("#tituloPersonal").css("margin-right","0pt");
      $("#tituloPersonal").parent().css("text-align","center");
    }
    else{
      $("#selectDiaPersonal").select2({
          theme: "bootstrap"
      });
      $("#tituloPersonal").css("margin-right","30pt");
      $("#tituloPersonal").parent().css("text-align","right");
    }
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
      $.ajax({
        url:   'controller/datosDiasCantidad.php',
        type:  'post',
        success: function (response2) {
          var pGraf = jQuery.parseJSON(response2);
          if(pGraf.aaData.length !== 0){
            google.charts.load('current', {packages: ['corechart']});
            function drawChart() {
              var data = new google.visualization.DataTable();

              data.addColumn('string', 'Día');
              data.addColumn('number', 'AM');
              data.addColumn({type:'string', role:'annotation'});
              data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
              data.addColumn('number', 'PM');
              data.addColumn({type:'string', role:'annotation'});
              data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});

              var max = 0;

              for(var i = 0; i < pGraf.aaData.length; i++){
                if(max < parseFloat(pGraf.aaData[i].CANTIDAD_AM)){
                  max = parseFloat(pGraf.aaData[i].CANTIDAD_AM);
                }
                if(max < parseFloat(pGraf.aaData[i].CANTIDAD_PM)){
                  max = parseFloat(pGraf.aaData[i].CANTIDAD_PM);
                }
                data.addRows([[
                  pGraf.aaData[i].DIA,
                  parseFloat(pGraf.aaData[i].CANTIDAD_AM),
                  pGraf.aaData[i].CANTIDAD_AM,
                  createCustomHTMLContent('AM', pGraf.aaData[i].DIA, pGraf.aaData[i].CANTIDAD_AM),
                  parseFloat(pGraf.aaData[i].CANTIDAD_PM),
                  pGraf.aaData[i].CANTIDAD_PM,
                  createCustomHTMLContent('PM', pGraf.aaData[i].DIA, pGraf.aaData[i].CANTIDAD_PM),
                ]]);
              }
              max = (max + max/2);

              if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                var h = 300;
                var options = {
                  title: null,
                  titleTextStyle: {
                      fontSize: 18
                  },
                  tooltip: {
                      isHtml: true
                  },
                  isStacked: false,
                  annotations: {
                      textStyle: {
                          fontSize: 12,
                          bold: true,
                      }
                  },
                  vAxis: {
                      textStyle: {
                          fontSize: 14,
                          bold: true,
                      },
                      viewWindow: {
                          min: 0,
                          max: max,
                      },
                      format: 'short'
                  },
                  legend: {
                    position: 'bottom'
                  },
                  hAxis: {
                      textStyle: {
                          fontSize: 9,
                      },
                      slantedText: true,
                      slantedTextAngle: 70
                  },
                  animation:{
                      duration: 1000,
                      easing: 'out',
                  },
                  backgroundColor:{
                      fill: "#ffffff"
                  },
                  // series: {
                  //     0: {
                  //         type: 'line', lineDashStyle: [2,4],
                  //         lineWidth: 2,
                  //         pointsVisible: true
                  //     },
                  //     1: {
                  //         type: 'line', lineDashStyle: [2,4],
                  //         lineWidth: 2,
                  //         pointsVisible: true
                  //     }
                  // },
                  'height':  h,
                  'chartArea': {
                      'right': '0',
                      'left': '0',
                      'top': '40',
                      'width': '100%',
                      'height': '60%'
                  }
                };
              }
              else{
                var h = parseInt($("#menu-lateral").height()/2);
                var options = {
                  title: null,
                  titleTextStyle: {
                      fontSize: 18
                  },
                  tooltip: {
                      isHtml: true
                  },
                  isStacked: false,
                  annotations: {
                      textStyle: {
                          fontSize: 12,
                          bold: true,
                      }
                  },
                  vAxis: {
                      textStyle: {
                          fontSize: 14,
                          bold: true,
                      },
                      viewWindow: {
                          min: 0,
                          max: max,
                      },
                      format: 'short'
                  },
                  legend: {
                    position: 'bottom'
                  },
                  hAxis: {
                      textStyle: {
                          fontSize: 9,
                      },
                      slantedText: false
                      // slantedTextAngle: 40
                  },
                  animation:{
                      duration: 1000,
                      easing: 'out',
                  },
                  backgroundColor:{
                      fill: "#ffffff"
                  },
                  // series: {
                  //     0: {
                  //         type: 'line', lineDashStyle: [2,4],
                  //         lineWidth: 2,
                  //         pointsVisible: true
                  //     },
                  //     1: {
                  //         type: 'line', lineDashStyle: [2,4],
                  //         lineWidth: 2,
                  //         pointsVisible: true
                  //     }
                  // },
                  'pointsVisible': true,
                  'lineDashStyle': [2,4],
                  'lineWidth': 2,
                  'selectionMode': 'multiple',
                  'height':  h,
                  'chartArea': {
                      'right': '20',
                      'left': '40',
                      'top': '40',
                      'width': '100%',
                      'height': '60%'
                  }
                };
              }

              var chart = new google.visualization.LineChart(
                document.getElementById('graficaPersonal')
              );
              chart.draw(data, options);
            }

            var largo = Math.trunc(($("#menu-lateral").height() - ($(window).height()/100)*50)/30);
            var formattedDate = new Date();
            var d = formattedDate.getDate();
            var m =  formattedDate.getMonth();
            m += 1;  // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            if(m < 10){
              m = '0' + m;
            }
            if(d < 10){
              d = '0' + d;
            }

            var fecha = y + "-" + m + "-" + d;

            var parametros = {
              "fecha": fecha
            }
            $.ajax({
              url:   'controller/datosDiasPersonal.php',
              type:  'post',
              success: function (response3) {
                var pDias = jQuery.parseJSON(response3);
                if(pDias.aaData.length !== 0){
                  var cuerpo = '';
                  var a = 0;
                  for(var z = 0; z < pDias.aaData.length; z++){
                    if(fecha == pDias.aaData[z].FECHA){
                      a = 1;
                    }
                  }
                  if(a == 0){
                    cuerpo += '<option value="' + fecha +  '">' + fecha + '</option>';
                  }
                  for(var z = 0; z < pDias.aaData.length; z++){
                    cuerpo += '<option value="' + pDias.aaData[z].FECHA +  '">' + pDias.aaData[z].FECHA + '</option>';
                  }
                  $("#selectDiaPersonal").html(cuerpo);
                }
              }
            });
            $('#tablaPersonal').DataTable( {
                ajax: {
                    url: "controller/datosPersonal.php",
                    type: 'POST',
                    data: parametros,
                },
                columns: [
                    { data: 'DNI' } ,
                    { data: 'NOMBRE' },
                    { data: 'MAIL' },
                    { data: 'FONO' },
                    { data: 'EMPRESA' },
                    { data: 'TERMO_VS' , className: "centerDataTable"},
                    { data: 'TAM' , className: "centerDataTable"},
                    { data: 'TPM' , className: "centerDataTable"},
                    { data: 'TERMOMETRO' , className: "never"}
                ],
                responsive: true,
                buttons: [
                  {
                      extend: 'excel',
                      exportOptions: {
                        columns: [0,1,2,3,4,6,7,8]
                      },
                      title: null,
                      text: 'Excel'
                  }
                ],
                select: {
                    style: 'single'
                },
                "scrollX": true,
                "paging": true,
                "ordering": true,
                "scrollCollapse": true,
                "order": [[ 0, "asc" ]],
                "info":     true,
                "lengthMenu": [[largo], [largo]],
                "dom": 'Bfrtip',
                "language": {
                    "zeroRecords": "No tiene personal bajo su cargo",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No tiene personal bajo su cargo",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: ",
                    "select": {
                        "rows": "- %d registros seleccionados"
                    },
                    "infoFiltered": "(Filtrado de _MAX_ registros)"
                },
                "destroy": true,
                "autoWidth": false,
                "initComplete": function( settings, json){
                  $('#contenido').fadeIn();
                  $('#footer').fadeIn();
                  $('#menu-lateral').fadeIn();
                  $.ajax({
                    url:   'controller/datosAreasComunes.php',
                    type:  'post',
                    success: function (response2) {
                      var p2 = jQuery.parseJSON(response2);
                      if(p2.aaData.length !== 0){

                        for(var i = 0; i < p2.aaData.length; i++){
                          if(p2.aaData[i].NOMBRE == 'buttonMiPersonal'){
                            if(p2.aaData[i].TODOS == '1'){
                              $("#sintomasPersonalTermometro").css("display","inline");
                            }
                            else{
                              $("#sintomasPersonalTermometro").css("display","none");
                            }
                          }
                        }
                      }
                    }
                  });
                  setTimeout(function(){
                      $("#logoLinkWeb").fadeOut();
                      $("#logoMenu").fadeOut();
                      $("#lineaMenu").fadeOut();
                      $.ajax({
                        url:   'controller/datosAreas.php',
                        type:  'post',
                        success: function (response2) {
                          var p2 = jQuery.parseJSON(response2);
                          if(p2.aaData.length !== 0){
                            for(var i = 0; i < p2.aaData.length; i++){
                              $('div[id *=' + p2.aaData[i].PADRE + ']').css("display","none");
                              $('li[id *=' + p2.aaData[i].NOMBRE + ']').css("display","none");
                            }
                          }
                        }
                      });
                      $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                      $("#menu-lateral").css("width","45px");
                      $("#logoMenu").fadeIn();
                      $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                      $("#logoMenu").css("color","black");
                      $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                      $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                      $('#modalAlertasSplash').modal('hide');
                  },500);
                  new ResizeSensor(jQuery('#divGraficaPersonal'), function(){
                    google.charts.setOnLoadCallback(drawChart);
                  });
                  new ResizeSensor(jQuery('#divTablaPersonal'), function(){
                    $('#tablaPersonal').DataTable().columns.adjust();
                  });
                }
            });
          }
        }
      });
    },1500);
});

app.controller("seguimientoController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');

    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    $('#contenido').fadeIn();
    $('#footer').fadeIn();
    $('#menu-lateral').fadeIn();
    setTimeout(function(){
        $('#modalAlertasSplash').modal('hide');
        $('#tablaPersonal').DataTable().columns.adjust();
    },500);
});

//funciones
function formatNumber(num) {
  if (!num || num == 'NaN') return '-';
  if (num == 'Infinity') return '&#x221e;';
  num = num.toString().replace(/\$|\,/g, '');
  if (isNaN(num))
      num = "0";
  sign = (num == (num = Math.abs(num)));
  num = Math.floor(num * 100 + 0.50000000001);
  num = Math.floor(num / 100).toString();
  for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
      num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
  return (((sign) ? '' : '-') + num);
}

function createCustomHTMLContent(titulo, texto, valor){
 return  '<div style="padding: 15px 15px 15px 15px; width: 100px; height: 100px; text-align: center;">' +
          '<font style="font-weight: bold; font-size: 13px;">' +
          titulo +
          '</font>' +
          '<br/><br/>' +
          '<font style="font-size: 13px;">' + texto + '</font><br/>' +
          '<font style="font-weight: bold; font-size: 13px;">' +
          formatNumber(valor) +
          '</font>' +
          '<br/>' +
          '</div>';
}
