<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');

	//Login
	function consultaUsuarioConectado($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT
CASE WHEN TIMESTAMPDIFF(SECOND , TOKEN_WEB_TIME, NOW()) < 300 THEN 'SI' ELSE 'NO' END 'CHECK'
FROM USUARIO
WHERE RUT = '{$rut}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuario($rut, $pass){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '{$rut}' AND PASS = '{$pass}'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Refresh
	function checkUsuarioSinPass($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '{$rut}'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Ingresa Token Login
	function actualizaTokenLogin($rut, $token){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = '{$token}',
TOKEN_WEB_TIME = NOW()
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Destruye Token Login
	function destruyeTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL,
TOKEN_WEB_TIME = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Chequeo Token
	function checkToken($token){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT
FROM USUARIO
WHERE TOKEN_WEB  = '{$token}'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Borra Token Login
	function borraTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreasComunes($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO, 0 'TODOS'
FROM AREAWEB
WHERE TIPO = 1
UNION ALL
SELECT A.NOMBRE, A.PADRE, A.TIPO, P.TODOS 'TODOS'
FROM AREAWEB A
LEFT JOIN PERMISOS P
ON A.IDAREAWEB = P.IDAREAWEB
LEFT JOIN USUARIO U
ON P.IDUSUARIO = U.IDUSUARIO
WHERE A.TIPO = 0
AND U.RUT = '{$rut}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas padres
	function consultaAreasComunesPadre($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT PADRE
FROM AREAWEB
WHERE PADRE IN
(
SELECT A.PADRE
FROM AREAWEB A
LEFT JOIN PERMISOS P
ON A.IDAREAWEB = P.IDAREAWEB
LEFT JOIN USUARIO U
ON P.IDUSUARIO = U.IDUSUARIO
WHERE A.TIPO = 0
AND U.RUT = '{$rut}'
)
UNIOn ALL
SELECT DISTINCT PADRE
FROM AREAWEB
WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Personal
	function consultaPersonalTodos($fecha){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL PERSONAL_LISTA_TODOS('{$fecha}')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Personal todos
	function consultaPersonal($rut,$fecha){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL PERSONAL_LISTA('{$rut}','{$fecha}')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Datos Fancy Term
	function consultaFancyDatosterm($id){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT TEMPERATURA, TELEFONO, EMAIL, NULL 'FOTO'
FROM PERSONAL_INGS
WHERE IDPERSONAL_INGS = {$id}";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Datos Fancy Term
	function consultaFancyDatostermFoto($id){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT FOTO
FROM PERSONAL_INGS
WHERE IDPERSONAL_INGS = {$id}";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Consulta sintomas
	function consultaPersonalSintomasTodos($fecha){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL SINTOMAS_TODOS('{$fecha}')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaPersonalSintomas($fecha,$rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL SINTOMAS('{$rut}','{$fecha}')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Actualiza estado termometro
	function updateEstadoTermometro($rut, $estado){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE PERSONAL
SET TERMOMETRO = '{$estado}'
WHERE DNI = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Correcto";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function consultaDiasCantidad($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL DIAS_CANTIDAD('{$rut}')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDiasCantidadTodos(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL DIAS_CANTIDAD_TODOS()";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDiasDatosPersonal(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DATE_FORMAT(P.FECHAHORA, \"%y%y-%m-%d\") 'FECHA'
FROM PERSONAL_INGS P
WHERE DATE_FORMAT(P.FECHAHORA, \"%y%y-%m-%d\")
>=
DATE_ADD(DATE_FORMAT(NOW(), \"%y%y-%m-%d\"), INTERVAL -6 DAY)
GROUP BY DATE_FORMAT(P.FECHAHORA, \"%y%y-%m-%d\")
ORDER BY DATE_FORMAT(P.FECHAHORA, \"%y%y-%m-%d\")  DESC";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

?>
